
package Servlet;

import DAO.DAORequest;
import DAO.DAOTask;
import Entity.Response.ResponseAPI;
import Entity.Response.TaskToAdd;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tung 2508
 */
public class AddTask extends ServletBase {
    Gson gson = new Gson();
    StringBuilder bu = new StringBuilder();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ResponseAPI<String> resAPI = new ResponseAPI<>();
        
        try {
            boolean n = false;
            bu = new StringBuilder();
            
            String line = "";
            BufferedReader reader = req.getReader();
            DAORequest reqDao = new DAORequest();
            while((line = reader.readLine()) != null){
                bu.append(line).append("\n");
            }
            TaskToAdd addTa = new TaskToAdd();
            addTa = gson.fromJson(bu.toString(), TaskToAdd.class);
            if(addTa != null){
                DAOTask taskDao = new DAOTask();
                n = taskDao.editTask(addTa.getRequestId(), addTa.getTitle(), addTa.getDescription());
            }
            if(n){
                resAPI.setCode(200);
                resAPI.setMessage("Success");
            }else{
                resAPI.setCode(400);
                resAPI.setMessage("Failed");
            }
            String data = gson.toJson(resAPI);
            writeResponse(req,data, resp);
            return;
            
        } catch (Exception e) {
            e.printStackTrace();
            resAPI.setCode(500);
            resAPI.setMessage("Failed");
            String data = gson.toJson(resAPI);
            writeResponse(req,data,resp);
        }
    }


}
