/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.DAOComment;
import DAO.DAORequest;
import Entity.Response.AddNewComment;
import Entity.Response.CreateReq;
import Entity.Response.ResponseAPI;
import Entity.Response.TaskCreateReq;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tung 2508
 */
public class AddComment extends ServletBase {
    Gson gson = new Gson();
    StringBuilder bu = new StringBuilder();
   
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ResponseAPI<String> resAPI = new ResponseAPI<>();
        try {
            int reqId = Integer.parseInt(req.getParameter("idReq"));
            boolean n = false;
            bu = new StringBuilder();
            
            String line = "";
            BufferedReader reader = req.getReader();
            DAORequest reqDao = new DAORequest();
            while((line = reader.readLine()) != null){
                bu.append(line).append("\n");
            }
            AddNewComment addCom = new AddNewComment();
            addCom = gson.fromJson(bu.toString(), AddNewComment.class);
            if(addCom != null){
                DAOComment comDao = new DAOComment();
                n = comDao.addComment(reqId,addCom.getEmployeeId(), addCom.getContent());
            }
            if(n){
                resAPI.setCode(200);
                resAPI.setMessage("Success");
            }else{
                resAPI.setCode(400);
                resAPI.setMessage("Failed");
            }
            String data = gson.toJson(resAPI);
            writeResponse(req,data, resp);
            return;
            
        } catch (Exception e) {
            e.printStackTrace();
            resAPI.setCode(500);
            resAPI.setMessage("Failed");
            String data = gson.toJson(resAPI);
            writeResponse(req,data,resp);
        }
        
    }


}
