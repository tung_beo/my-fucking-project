/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.DAORequest;
import Entity.Response.RequestToday;
import Entity.Response.ResponseAPI;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tung 2508
 */
public class getAllRequestToday extends ServletBase {
    Gson gson = new Gson();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ResponseAPI<List<RequestToday>> responseAPI = new ResponseAPI<>();
        try {
            int currPage = Integer.parseInt(req.getParameter("currPage"));
            int pageSize = Integer.parseInt(req.getParameter("pageSize"));
            List<RequestToday> list =  new ArrayList<>();
            
            DAORequest reqDao = new DAORequest();
            list = reqDao.getListAllReqToday(currPage, pageSize);
            int count = reqDao.countAllRequestToday();
            if(list != null){
                responseAPI.setCode(200);
                responseAPI.setMessage("Success");
                responseAPI.setData(list);
                responseAPI.setTotalPage(count);
            }else{
                responseAPI.setCode(400);
                responseAPI.setMessage("List null");
            }
            String data = gson.toJson(responseAPI);
            writeResponse(req,data, resp);
            return;
        } catch (Exception e) {
            responseAPI.setCode(500);
            responseAPI.setMessage("Error");
            String data = gson.toJson(responseAPI);
            writeResponse(req,data, resp);
            return;
        }
    
    }

}
