
package Servlet;

import DAO.DAORequest;
import Entity.Response.RequestUpdate;
import Entity.Response.ResponseAPI;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tung 2508
 */
public class UpdateRequestById extends ServletBase {

    Gson gson = new Gson();
    StringBuilder bu = new StringBuilder();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ResponseAPI<String> resAPI = new ResponseAPI<>();
        try {
            int reqId = Integer.parseInt(req.getParameter("id"));
            String line = "";
            BufferedReader reader = req.getReader();
            while((line = reader.readLine()) != null){
                bu.append(line);
            }
            System.out.println(""+reqId);
            RequestUpdate reqUp = new RequestUpdate();
            reqUp = gson.fromJson(bu.toString(), RequestUpdate.class);
            
            DAORequest reqDao = new DAORequest();
            if(reqDao.updateReqById(reqUp) > 0){
                resAPI.setCode(200);
                resAPI.setMessage("Success");
            }else{
                resAPI.setCode(400);
                resAPI.setMessage("Failed");
            }
            String data = gson.toJson(resAPI);
            writeResponse(req,data, resp);
            return;
        } catch (Exception e) {
            resAPI.setCode(500);
            resAPI.setMessage("Failed");
            String data = gson.toJson(resAPI);
            writeResponse(req,data,resp);
        }
    }

}
