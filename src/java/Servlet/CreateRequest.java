
package Servlet;

import DAO.DAORequest;
import Entity.Response.CreateReq;
import Entity.Response.ResponseAPI;
import Entity.Response.TaskCreateReq;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tung 2508
 */
public class CreateRequest extends ServletBase {
    Gson gson = new Gson();
    StringBuilder bu = new StringBuilder();
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ResponseAPI<String> resAPI = new ResponseAPI<>();
        
        try {
            int n = 0;
            bu = new StringBuilder();
            CreateReq creReq = new CreateReq();
            String line = "";
            BufferedReader reader = req.getReader();
            DAORequest reqDao = new DAORequest();
            while((line = reader.readLine()) != null){
                bu.append(line).append("\n");
            }
            creReq = gson.fromJson(bu.toString(), CreateReq.class);
            if(creReq != null){
                    if(reqDao.createReq(creReq) > 0){
                        for (TaskCreateReq task : creReq.getTasks()) {
                         n += reqDao.createTask(task);
                        }
                    }
            }
            if(n > 0){
                resAPI.setCode(200);
                resAPI.setMessage("Success");
            }else{
                resAPI.setCode(400);
                resAPI.setMessage("Failed");
            }
            String data = gson.toJson(resAPI);
            writeResponse(req,data, resp);
            return;
            
        } catch (Exception e) {
            e.printStackTrace();
            resAPI.setCode(500);
            resAPI.setMessage("Failed");
            String data = gson.toJson(resAPI);
            writeResponse(req,data,resp);
        }
    }
}
