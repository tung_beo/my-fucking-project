/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.DAOComment;
import DAO.DAORequest;
import DAO.DAOTask;
import Entity.Response.AddNewComment;
import Entity.Response.ResponseAPI;
import Entity.Response.TaskToUpdate;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tung 2508
 */
public class UpdateTask extends ServletBase {
    Gson gson = new Gson();
    StringBuilder bu = new StringBuilder();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ResponseAPI<String> resAPI = new ResponseAPI<>();
        try {
            int empId = Integer.parseInt(req.getParameter("id"));
            boolean n = false;
            bu = new StringBuilder();
            
            String line = "";
            BufferedReader reader = req.getReader();
            DAORequest reqDao = new DAORequest();
            while((line = reader.readLine()) != null){
                bu.append(line).append("\n");
            }
            TaskToUpdate upTa = new TaskToUpdate();
            upTa = gson.fromJson(bu.toString(), TaskToUpdate.class);
            if(upTa != null){
                DAOTask taskDao = new DAOTask();
                n = taskDao.editTask(empId, upTa.getTitle(), upTa.getDescription());
            }
            if(n){
                resAPI.setCode(200);
                resAPI.setMessage("Success");
            }else{
                resAPI.setCode(400);
                resAPI.setMessage("Failed");
            }
            String data = gson.toJson(resAPI);
            writeResponse(req,data, resp);
            return;
            
        } catch (Exception e) {
            e.printStackTrace();
            resAPI.setCode(500);
            resAPI.setMessage("Failed");
            String data = gson.toJson(resAPI);
            writeResponse(req,data,resp);
        }
    }

}
