/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.DAOComment;
import DAO.DAORequest;
import DAO.DAOTask;
import Entity.Basic.Comment;
import Entity.Basic.Task;
import Entity.Response.RequestById;
import Entity.Response.ResponseAPI;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tung 2508
 */
public class getReqById extends ServletBase {

    Gson gson = new Gson();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ResponseAPI<RequestById> responseAPI = new ResponseAPI<>();
        try {
            int reqId = Integer.parseInt(req.getParameter("id"));
            
            DAORequest reqDao = new DAORequest();
            DAOTask taskDao = new DAOTask();
            DAOComment cmtDao = new DAOComment();
            List<Task> tasks = new ArrayList<>();
            List<Comment> comments = new ArrayList<>();
            RequestById reqById = new RequestById();
            
            reqById = reqDao.getReqById(reqId);
            tasks = taskDao.getTaskByReqId(reqId);
            comments = cmtDao.getListCommentByReqId(reqId);
            
            if(reqById != null){
                responseAPI.setCode(200);
                responseAPI.setMessage("Success");
                reqById.setTasks(tasks);
                reqById.setComments(comments);
                responseAPI.setData(reqById);
            }else{
                responseAPI.setCode(400);
                responseAPI.setMessage("Request null");
            }
            String data = gson.toJson(responseAPI);
            writeResponse(req,data, resp);
            return;
        } catch (Exception e) {
            responseAPI.setCode(500);
            responseAPI.setMessage("Error");
            String data = gson.toJson(responseAPI);
            writeResponse(req,data, resp);
            return;
        }
    }

}
