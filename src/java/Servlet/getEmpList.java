
package Servlet;

import DAO.DAOEmployee;
import Entity.Basic.Employee;
import Entity.Response.ResponseAPI;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class getEmpList extends ServletBase {
    Gson gson = new Gson();
    protected void processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        DAOEmployee daoEmp = new DAOEmployee();
        ResponseAPI<List<Employee>> responseAPI = new ResponseAPI<>();
        try{
            List<Employee> list = daoEmp.getLstEmployee();
            if(list != null){
                responseAPI.setCode(200);
                responseAPI.setMessage("Success");
                responseAPI.setData(list);
            }else{
                responseAPI.setCode(400);
                responseAPI.setMessage("List null");
            }
            String data = gson.toJson(responseAPI);
            writeResponse(req,data, resp);
            return;
        }catch(Exception e){
            responseAPI.setCode(500);
            responseAPI.setMessage("Error");
            String data = gson.toJson(responseAPI);
            writeResponse(req,data, resp);
            return;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
