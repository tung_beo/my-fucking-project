
package DAO;

import Entity.Basic.Comment;
import Entity.Basic.Task;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tung 2508
 */
public class DAOComment extends DAOBase{
    public List<Comment> getListCommentByReqId(int reqId) throws SQLException {
        List<Comment> lst = new ArrayList<>();
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        
        String sql = "select CommentID,e.Firstname+' '+e.Lastname 'name',Content,c.createDate from Comment c\n" +
                     "join Employees e on e.EmployeeID = c.EmployeeID\n" +
                     "where RequestID = "+reqId;
        try {
            con = getConnection();
            pre = con.prepareStatement(sql);
            rs = pre.executeQuery();
            while(rs.next()){
                Comment req = new Comment();
                req.setId(Integer.parseInt(rs.getString("CommentID")));
                req.setEmpName(rs.getString("name"));
                req.setContent(rs.getString("Content"));
                req.setCreateDate(rs.getDate("createDate"));
                lst.add(req);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
        
        return lst;
    }

    public boolean addComment(int RequestID, int EmployeeID, String Content) throws SQLException {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        int result = 0;
        String sql = "INSERT INTO [dbo].[Comment]\n" +
                "           ([RequestID]\n" +
                "           ,[EmployeeID]\n" +
                "           ,[Content])\n" +
                "     VALUES\n" +
                "           (?\n" +
                "           ,?\n" +
                "           ,?)";

        try{
            con = getConnection();
            pre = con.prepareStatement(sql);
            pre.setInt(1, RequestID);
            pre.setInt(2, EmployeeID);
            pre.setString(3, Content);
            result = pre.executeUpdate();
            if (result!=0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }

        return false;
    }
}
