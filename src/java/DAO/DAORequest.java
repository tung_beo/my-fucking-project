/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Response.CreateReq;
import Entity.Response.RequestById;
import Entity.Response.RequestToday;
import Entity.Response.RequestUpdate;
import Entity.Response.TaskCreateReq;
import Entity.Response.listAllRequest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tung 2508
 */
public class DAORequest extends DAOBase{
    //get list all requests
    public List<listAllRequest> getListAllRequest(int currPage, int pageSize) throws SQLException{
        List<listAllRequest> lst = new ArrayList<>();
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        int firstRow = currPage*pageSize - pageSize + 1;
        int lastRow = currPage*pageSize;
        
        String sql = "select * from (select ROW_NUMBER() over(order by RequestID) as rn, a.* from"
                + "(select distinct re.RequestID,e.Firstname+' '+e.Lastname 'name', \n"
                + "e.Department,case when e.PermissionType = 'e' then 'employee' else 'boss' end as possition, \n"
                + "re.createDate,re.modifiedDate,re.seenDate,re.isAllow,count(rt.TaskID) 'task_total', \n"
                + "(select count(TaskID) from RequestTask where [Status] = 'done' and RequestID= re.RequestID) 'task_done' \n"
                + "from Requests re \n"
                + "left join Employees e on re.EmployeeID = e.EmployeeID \n"
                + "left join RequestTask rt on re.RequestID = rt.RequestID \n"
                + "group by re.RequestID,e.Firstname,e.Lastname,e.Department,e.PermissionType, \n"
                + "re.createDate,re.modifiedDate,re.seenDate,re.isAllow)a)b \n"
                + "where rn >= "+firstRow+" and rn <="+lastRow;
        try {
            con = getConnection();
            pre = con.prepareStatement(sql);
            rs = pre.executeQuery();
            while(rs.next()){
                listAllRequest req = new listAllRequest();
                req.setId(Integer.parseInt(rs.getString("RequestID")));
                req.setName(rs.getString("name"));
                req.setDepartment(rs.getString("department"));
                req.setPossition(rs.getString("possition"));
                req.setCreateAt(rs.getDate("createDate"));
                req.setRequestAt(rs.getDate("modifiedDate"));
                req.setAcceptAt(rs.getDate("seenDate"));
                req.setIsAccept(rs.getString("isAllow").equalsIgnoreCase("true")? true : false);
                req.setTotal(Integer.parseInt(rs.getString("task_total")));
                req.setDone(Integer.parseInt(rs.getString("task_done")));
                lst.add(req);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
        
        return lst;
    }
    // count all request
    public int countGetListAllRequest() throws SQLException{
        int count = 0;
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        String sql = "select count(*) 'total' from"
                + "(select distinct re.RequestID,e.Firstname+' '+e.Lastname 'name', \n"
                + "e.Department,case when e.PermissionType = 'e' then 'employee' else 'boss' end as possition, \n"
                + "re.createDate,re.modifiedDate,re.seenDate,re.isAllow,count(rt.TaskID) 'task_total', \n"
                + "(select count(TaskID) from RequestTask where [Status] = 'done' and RequestID= re.RequestID) 'task_done' \n"
                + "from Requests re \n"
                + "left join Employees e on re.EmployeeID = e.EmployeeID \n"
                + "left join RequestTask rt on re.RequestID = rt.RequestID \n"
                + "group by re.RequestID,e.Firstname,e.Lastname,e.Department,e.PermissionType, \n"
                + "re.createDate,re.modifiedDate,re.seenDate,re.isAllow) a \n";
        try {
            con = getConnection();
            pre = con.prepareStatement(sql);
            rs = pre.executeQuery();
            if(rs.next())
            {
                count = Integer.parseInt(rs.getString("total"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
        return count;
    }
    
    // get list all request to day
    public List<RequestToday> getListAllReqToday(int currPage, int pageSize) throws SQLException{
        List<RequestToday> lst = new ArrayList<>();
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        int firstRow = currPage*pageSize - pageSize + 1;
        int lastRow = currPage*pageSize;
        
        String sql = "select * from (select ROW_NUMBER() over(order by RequestID) as rn, a.* from"
                + "(select distinct re.RequestID, e.EmployeeID,e.Firstname+ ' '+e.Lastname 'name',e.Department, \n"
                + "(select top 1 [status] from [check] where RequestID = re.RequestID order by Alarm asc) 'status', \n"
                + "(select top 1 alarm from [check] where RequestID = re.RequestID order by Alarm desc) 'updateAt' \n"
                + "from Requests re \n"
                + "join Employees e on re.EmployeeID = e.EmployeeID \n"
                + "join [Check] c on c.RequestID = re.RequestID \n"
                + "where FORMAT (re.StartDateEstimate, 'dd-MM-yy') = FORMAT (getdate(), 'dd-MM-yy'))a)b \n"
                + "where rn >= "+firstRow+" and rn <="+lastRow;
        try {
            con = getConnection();
            pre = con.prepareStatement(sql);
            rs = pre.executeQuery();
            while(rs.next()){
                RequestToday req = new RequestToday();
                req.setId(Integer.parseInt(rs.getString("RequestID")));
                req.setEmpName(rs.getString("name"));
                req.setDepartment(rs.getString("department"));
                req.setStatus(rs.getString("status").equalsIgnoreCase("Not yet")? false : true);
                req.setUpdateAt(rs.getString("updateAt"));
                lst.add(req);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
        
        return lst;
    }
    
    //count all request today
    public int countAllRequestToday() throws SQLException{
        int count = 0;
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        String sql = "select count(*) 'total' from"
                + "(select distinct re.RequestID, e.EmployeeID,e.Firstname+ ' '+e.Lastname 'name',e.Department, \n"
                + "(select top 1 [status] from [check] where RequestID = re.RequestID order by Alarm asc) 'status', \n"
                + "(select top 1 alarm from [check] where RequestID = re.RequestID order by Alarm desc) 'updateAt' \n"
                + "from Requests re \n"
                + "join Employees e on re.EmployeeID = e.EmployeeID \n"
                + "join [Check] c on c.RequestID = re.RequestID \n"
                + "where FORMAT (re.StartDateEstimate, 'dd-MM-yy') = FORMAT (getdate(), 'dd-MM-yy'))a \n";
        try {
            con = getConnection();
            pre = con.prepareStatement(sql);
            rs = pre.executeQuery();
            if(rs.next())
            {
                count = Integer.parseInt(rs.getString("total"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            con.close();
        }
        return count;
    }
    
    public RequestById getReqById(int id) throws SQLException {
        RequestById req = new RequestById();
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        String sql = "select re.RequestID,e.Firstname+' '+e.Lastname 'name',e.Department,\n" +
                    "e.PermissionType,case when e.PermissionType = 'e' then 'employee' else 'boss' end as possition,\n" +
                    "re.Reason,re.createDate,re.modifiedDate,re.seenDate,isAllow,timeremind \n" +
                    "(select count(TaskID) from RequestTask where RequestID =  re.RequestID) 'total',\n" +
                    "(select count(TaskID) from RequestTask where lower(Status) = 'done' and RequestID =  re.RequestID) 'done'\n" +
                    "from Requests re\n" +
                    "join Employees e on re.EmployeeID = e.EmployeeID\n" +
                    "where re.RequestID = "+id;
        try {
            con = getConnection();
            pre = con.prepareStatement(sql);
            rs = pre.executeQuery();
            if(rs.next())
            {
                req.setId(Integer.parseInt(rs.getString("RequestID")));
                req.setEmpName(rs.getString("name"));
                req.setDepartment(rs.getString("Department"));
                req.setPossition(rs.getString("possition"));
                req.setReason(rs.getString("reason"));
                req.setCreateAt(rs.getDate("createDate"));
                req.setRequestAt(rs.getDate("modifiedDate"));
                req.setAcceptAt(rs.getDate("seenDate"));
                req.setIsAccept(rs.getString("isAllow").equalsIgnoreCase("accept")?true:false);
                req.setTotal(Integer.parseInt(rs.getString("total")));
                req.setDone(Integer.parseInt(rs.getString("done")));
                req.setTimeRemind(Integer.parseInt(rs.getString("timeremind")));
            }
        } catch (NumberFormatException | SQLException e) {
        } finally {
            con.close();
        }
        return req;
    }

    //update request by id
    public int updateReqById(RequestUpdate reqUp) throws SQLException {
        Connection con = null;
        PreparedStatement pre = null;
        int n = -1;
        String isAllow = reqUp.isIsAccept()? "ACCEPT" : "REJECT";
        String sql = "update Requests set isAllow = '"+isAllow + "', timeremind = "+reqUp.getTimeRemind()+" where RequestID = " + reqUp.getId();
        try {
            con = getConnection();
            pre = con.prepareStatement(sql);
            n = pre.executeUpdate();
        } catch (Exception e) {
        } finally {
            con.close();
        }
        return n;
    }

    public int createReq(CreateReq creReq){
        int n = 0;
        Connection con = null;
        PreparedStatement pre = null;
        
        String sql = "insert into Requests(EmployeeID,Reason,createDate,StartDateEstimate,EndDateEstimate,timeremind) \n" +
                     "values("+creReq.getEmployeeId()+",'"+creReq.getReason()+"',getdate(),getdate()+1,getdate()+1,"+creReq.getTimeRemind()+")";
        try {
            con = getConnection();
            pre = con.prepareStatement(sql);
            n = pre.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return n;
    }
    public int createTask(TaskCreateReq task){
        int n = 0;
        Connection con = null;
        PreparedStatement pre = null;
        
        String sql = "DECLARE @reqId INT, @taskId INT;\n" +
                    "begin\n" +
                    "	set @reqId = (select max(requestID) from  Requests);\n" +
                    "	insert into Tasks(Description) values('"+task.getDescription()+"');\n" +
                    "	set @taskId = (select max(TaskID) from Tasks);\n" +
                    "	insert into RequestTask(RequestID,TaskID) values(@reqId,@taskId);\n" +
                    "end";
        try {
            con = getConnection();
            pre = con.prepareStatement(sql);
            n = pre.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return n;
    }

    public int getTotalTask(int id) throws SQLException {
        int total=0;
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        String sql = "select count(TaskID) as total from RequestTask where RequestID =  ?";

        try{
            con = getConnection();
            pre = con.prepareStatement(sql);
            pre.setInt(1,id);
            rs = pre.executeQuery();
            if(rs.next())
            {
                total = rs.getInt("total");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            con.close();
        }
        return total;
    }

    public int getTotalDoneTask(int id) throws SQLException {
        int total=0;
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        String sql = "select count(TaskID) as total from RequestTask where lower(Status) = 'done' and RequestID =  ?";

        try{
            con = getConnection();
            pre = con.prepareStatement(sql);
            pre.setInt(1,id);
            rs = pre.executeQuery();
            if(rs.next())
            {
                total = rs.getInt("total");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            con.close();
        }
        return total;
    }
    
}
