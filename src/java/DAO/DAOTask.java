
package DAO;

import Entity.Basic.Task;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DAOTask extends DAOBase{
    
    public List<Task> getTaskByReqId(int reqId) throws SQLException {
        List<Task> lst = new ArrayList<>();
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        
        String sql = "select t.TaskID,t.Description,rt.Status from Tasks t \n" +
                     "join RequestTask rt on t.TaskID = rt.TaskID \n" +
                     "where rt.RequestID = "+reqId;
        try {
            con = getConnection();
            pre = con.prepareStatement(sql);
            rs = pre.executeQuery();
            while(rs.next()){
                Task req = new Task();
                req.setTaskId(Integer.parseInt(rs.getString("TaskID")));
                req.setTitle("Work From Home");
                req.setDescription(rs.getString("Description"));
                req.setStatus(rs.getString("Status").equalsIgnoreCase("done")?true:false);
                lst.add(req);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            con.close();
        }
        
        return lst;
    }

    public boolean editTask(int id, String title, String description) throws SQLException {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        int result = 0;
        String sql = "UPDATE [dbo].[Tasks]\n" +
                "   SET [Title] = ?\n" +
                "      ,[Description] = ?\n" +
                " WHERE [TaskID]=?";

        try{
            con = getConnection();
            pre = con.prepareStatement(sql);
            pre.setString(1,title);
            pre.setString(2,description);
            pre.setInt(3,id);
            result = pre.executeUpdate();
            if (result!=0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }

        return false;
    }
    
//    public boolean addTask( String title, String description) throws SQLException {
//        Connection con = null;
//        ResultSet rs = null;
//        PreparedStatement pre = null;
//        int result = 0;
//        String sql = "insert into Tasks(CreateByEmployeeID,Description) values";
//
//        try{
//            con = getConnection();
//            pre = con.prepareStatement(sql);
//            pre.setString(1,title);
//            pre.setString(2,description);
//            pre.setInt(3,id);
//            result = pre.executeUpdate();
//            if (result!=0) {
//                return true;
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            con.close();
//        }
//
//        return false;
//    }

}
