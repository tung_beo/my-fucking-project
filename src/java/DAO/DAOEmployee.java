/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;


import Entity.Basic.Employee;
import Entity.Response.WorkingTodayByEmpID;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tung 2508
 */
public class DAOEmployee extends DAOBase{
    
    public List<Employee> getLstEmployee() throws SQLException{
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        List<Employee> empList = new ArrayList<>();
        String sql = "select * from Employees ";
        try{
            con = getConnection();
            pre = con.prepareStatement(sql);
            System.out.println(sql);
            rs = pre.executeQuery();
            while(rs.next()){
                Employee emp = new Employee();
                emp.setId(rs.getInt("employeeid"));
                emp.setFirstName(rs.getString("firstname"));
                emp.setLastName(rs.getString("lastname"));
                emp.setDateOfBirth(rs.getDate("dob"));
                emp.setPermissionType(rs.getString("permissiontype"));
                empList.add(emp);
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            con.close();
        }
        return empList;
    }

    public WorkingTodayByEmpID getWorkingTodayByEmpID(int idEmp) throws SQLException {
        WorkingTodayByEmpID wtd = new WorkingTodayByEmpID();

        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;

        String query = "SELECT TOP (1000) request.[EmployeeID] as id, request.[RequestID],\n" +
                "employee.[Firstname] as firsname, employee.[Lastname] as lastname, employee.[Department] as department, employee.[PermissionType] as position,\n" +
                "request.[Reason] as reason, request.[isAllow] as isAccpet\n" +
                "FROM [hackathon].[dbo].[Employees] AS employee \n" +
                "INNER JOIN [hackathon].[dbo].[Requests] AS request ON employee.[EmployeeID]= request.[EmployeeID]\n" +
                "WHERE employee.[EmployeeID]=?\n" +
                "AND FORMAT(request.[StartDateEstimate], 'dd/MM/yyyy') = FORMAT(GETDATE(), 'dd/MM/yyyy')";

        try{
            con = getConnection();
            pre = con.prepareStatement(query);
            pre.setInt(1, idEmp);
            rs = pre.executeQuery();

            if(rs.next()){
                wtd.setId(rs.getInt("id"));
                wtd.setRequestId(rs.getInt("requestid"));
                wtd.setName(rs.getString("firsname") + " " + rs.getString("lastname"));
                wtd.setDepartment(rs.getString("department"));
                wtd.setPosition(rs.getString("position"));
                wtd.setReason(rs.getString("reason"));
                wtd.setIsAccept(rs.getString("isAccpet"));

                DAOComment com = new DAOComment();
                wtd.setComments(com.getListCommentByReqId(wtd.getRequestId()));

                DAOCheck ck = new DAOCheck();
                wtd.setLogs(ck.getCheckByRequestId(wtd.getRequestId()));

                DAOTask tk = new DAOTask();
                wtd.setTasks(tk.getTaskByReqId(wtd.getRequestId()));

                DAORequest rq = new DAORequest();
                wtd.setTotal(rq.getTotalTask(wtd.getRequestId()));

                wtd.setDone(rq.getTotalDoneTask(wtd.getRequestId()));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }

        return wtd;
    }
}
