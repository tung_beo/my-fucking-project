package DAO;

import Entity.Basic.Check;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOCheck extends DAOBase{

    public List<Check> getCheckByRequestId(int id) throws SQLException {
        List<Check> checklist = new ArrayList<>();
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;

        String query= "SELECT [CheckID],[RequestID],[Alarm],[CheckDate],[Status],[Data],[Comfirms],[ComfirmsDate]\n" +
                "FROM [hackathon].[dbo].[Check]\n" +
                "WHERE [RequestID]=?";

        try{
            con=getConnection();
            pre = con.prepareStatement(query);
            pre.setInt(1,id);
            rs=pre.executeQuery();
            while (rs.next()) {
                Check ck = new Check();
                ck.setId(rs.getInt("CheckID"));
                ck.setRequestId(rs.getInt("RequestID"));
                ck.setTimeChecked(rs.getDate("CheckDate"));
                ck.setAlarm(rs.getTime("Alarm"));
                ck.setStatus(rs.getString("Status"));
                ck.setImage(rs.getString("Data"));
                ck.setConfirm(rs.getBoolean("Comfirms"));
                ck.setConfirmDate(rs.getDate("ComfirmsDate"));
                checklist.add(ck);
            }
            return  checklist;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            con.close();
        }
        return null;
    }

    public boolean addCheck(int idRequest, Time alarm, String checkDate, String status, String image) throws SQLException {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        int result = 0;
        String sql = "INSERT INTO [dbo].[Check]\n" +
                "           ([RequestID]\n" +
                "           ,[Alarm]\n" +
                "           ,[CheckDate]\n" +
                "           ,[Status]\n" +
                "           ,[Data]\n" +
                "           ,[Comfirms])\n" +
                "     VALUES\n" +
                "           (?\n" +
                "           ,?\n" +
                "           ,?\n" +
                "           ,?\n" +
                "           ,?\n" +
                "           ,?)";

        try{
            con = getConnection();
            pre = con.prepareStatement(sql);
            pre.setInt(1, idRequest);
            pre.setTime(2, alarm);
            pre.setString(3, checkDate);
            pre.setString(4, status);
            pre.setString(5, image);
            pre.setBoolean(6,true);
            result = pre.executeUpdate();
            if (result!=0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            con.close();
        }

        return false;
    }
}
