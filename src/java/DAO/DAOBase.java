package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAOBase {
    private static final String URL = "jdbc:sqlserver://localhost:1433;database=hackathon";
    private static final String userName = "tungbeo";
    private static final String passWord = "tung beo";
    public Connection con;
    public DAOBase(String URL, String userName,String password){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con=DriverManager.getConnection(URL,userName,password);
            System.out.println("connected");
        }catch(ClassNotFoundException c){
            c.printStackTrace();
        }catch(SQLException e){
            e.printStackTrace();
        }

    }
    public DAOBase(){
        this(URL,userName,passWord);
    }
    public Connection getConnection() {
        return con;
    }

}
