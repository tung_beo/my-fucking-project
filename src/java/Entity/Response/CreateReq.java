
package Entity.Response;

import java.util.List;

/**
 *
 * @author Tung 2508
 */
public class CreateReq {
    int employeeId;
    String reason;
    String requestAt;
    int timeRemind;
    List<TaskCreateReq> tasks;

    public CreateReq() {
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getRequestAt() {
        return requestAt;
    }

    public void setRequestAt(String requestAt) {
        this.requestAt = requestAt;
    }

    public int getTimeRemind() {
        return timeRemind;
    }

    public void setTimeRemind(int timeRemind) {
        this.timeRemind = timeRemind;
    }

    public List<TaskCreateReq> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskCreateReq> tasks) {
        this.tasks = tasks;
    }

    @Override
    public String toString() {
        return "CreateReq{" +
                "employeeId=" + employeeId +
                ", reason='" + reason + '\'' +
                ", requestAt='" + requestAt + '\'' +
                ", timeRemind=" + timeRemind +
                ", tasks=" + tasks +
                '}';
    }
}
