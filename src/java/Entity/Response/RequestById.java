
package Entity.Response;

import Entity.Basic.Comment;
import Entity.Basic.Task;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Tung 2508
 */
public class RequestById {
    int id;
    String empName;
    String department;
    String possition;
    String reason;
    Date createAt;
    Date requestAt;
    Date acceptAt;
    boolean isAccept;
    int total;
    int done;
    int timeRemind;
    List<Task> tasks;
    List<Comment> comments;

    public RequestById() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPossition() {
        return possition;
    }

    public void setPossition(String possition) {
        this.possition = possition;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getRequestAt() {
        return requestAt;
    }

    public void setRequestAt(Date requestAt) {
        this.requestAt = requestAt;
    }

    public Date getAcceptAt() {
        return acceptAt;
    }

    public void setAcceptAt(Date acceptAt) {
        this.acceptAt = acceptAt;
    }

    public boolean isIsAccept() {
        return isAccept;
    }

    public void setIsAccept(boolean isAccept) {
        this.isAccept = isAccept;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getDone() {
        return done;
    }

    public void setDone(int done) {
        this.done = done;
    }

    public int getTimeRemind() {
        return timeRemind;
    }

    public void setTimeRemind(int timeRemind) {
        this.timeRemind = timeRemind;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "RequestById{" +
                "id=" + id +
                ", empName='" + empName + '\'' +
                ", department='" + department + '\'' +
                ", possition='" + possition + '\'' +
                ", reason='" + reason + '\'' +
                ", createAt=" + createAt +
                ", requestAt=" + requestAt +
                ", acceptAt=" + acceptAt +
                ", isAccept=" + isAccept +
                ", total=" + total +
                ", done=" + done +
                ", timeRemind=" + timeRemind +
                ", tasks=" + tasks +
                ", comments=" + comments +
                '}';
    }
}
