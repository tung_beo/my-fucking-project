
package Entity.Response;

import java.util.Date;

/**
 *
 * @author Tung 2508
 */
public class RequestToday {
    int id;
    String empName;
    String department;
    boolean status;
    String updateAt;
    int totalTask;
    int doneTask;

    public RequestToday() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public int getTotalTask() {
        return totalTask;
    }

    public void setTotalTask(int totalTask) {
        this.totalTask = totalTask;
    }

    public int getDoneTask() {
        return doneTask;
    }

    public void setDoneTask(int doneTask) {
        this.doneTask = doneTask;
    }

    @Override
    public String toString() {
        return "RequestToday{" +
                "id=" + id +
                ", empName='" + empName + '\'' +
                ", department='" + department + '\'' +
                ", status=" + status +
                ", updateAt='" + updateAt + '\'' +
                ", totalTask=" + totalTask +
                ", doneTask=" + doneTask +
                '}';
    }
}
