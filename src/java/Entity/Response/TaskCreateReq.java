/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity.Response;

/**
 *
 * @author Tung 2508
 */
public class TaskCreateReq {
    String title;
    String description;

    public TaskCreateReq() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "TaskCreateReq{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
