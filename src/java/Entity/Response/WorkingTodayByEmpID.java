package Entity.Response;

import Entity.Basic.Check;
import Entity.Basic.Comment;
import Entity.Basic.Task;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

public class WorkingTodayByEmpID {
    private int id;
    private int requestId;
    private String name;
    private String department;
    private String position;
    private String reason;
    private String isAccept;
    private int total;
    private int done;
    private Time timeRemind;
    private Date createdAt;
    private Date requestAt;
    private Date acceptAt;
    private List<Task> tasks;
    private List<Comment> comments;
    private List<Check> logs;

    public WorkingTodayByEmpID() {
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getIsAccept() {
        return isAccept;
    }

    public void setIsAccept(String isAccept) {
        this.isAccept = isAccept;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getDone() {
        return done;
    }

    public void setDone(int done) {
        this.done = done;
    }

    public Time getTimeRemind() {
        return timeRemind;
    }

    public void setTimeRemind(Time timeRemind) {
        this.timeRemind = timeRemind;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getRequestAt() {
        return requestAt;
    }

    public void setRequestAt(Date requestAt) {
        this.requestAt = requestAt;
    }

    public Date getAcceptAt() {
        return acceptAt;
    }

    public void setAcceptAt(Date acceptAt) {
        this.acceptAt = acceptAt;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Check> getLogs() {
        return logs;
    }

    public void setLogs(List<Check> logs) {
        this.logs = logs;
    }

    @Override
    public String toString() {
        return "WorkingTodayByEmpID{" +
                "id=" + id +
                ", requestId=" + requestId +
                ", name='" + name + '\'' +
                ", department='" + department + '\'' +
                ", position='" + position + '\'' +
                ", reason='" + reason + '\'' +
                ", isAccept='" + isAccept + '\'' +
                ", total=" + total +
                ", done=" + done +
                ", timeRemind=" + timeRemind +
                ", createdAt=" + createdAt +
                ", requestAt=" + requestAt +
                ", acceptAt=" + acceptAt +
                ", tasks=" + tasks +
                ", comments=" + comments +
                ", logs=" + logs +
                '}';
    }
}
