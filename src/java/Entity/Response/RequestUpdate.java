
package Entity.Response;

import java.util.Date;


public class RequestUpdate {
    int id;
    String name;
    String department;
    String position;
    String reason;
    String createdAt;
    String requestAt;
    String acceptAt;
    boolean isAccept;
    int timeRemind;

    public RequestUpdate() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmpName() {
        return name;
    }

    public void setEmpName(String empName) {
        this.name = empName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPossition() {
        return position;
    }

    public void setPossition(String possition) {
        this.position = possition;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean isIsAccept() {
        return isAccept;
    }

    public void setIsAccept(boolean isAccept) {
        this.isAccept = isAccept;
    }

    public int getTimeRemind() {
        return timeRemind;
    }

    public void setTimeRemind(int timeRemind) {
        this.timeRemind = timeRemind;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateAt() {
        return createdAt;
    }

    public void setCreateAt(String createAt) {
        this.createdAt = createAt;
    }

    public String getRequestAt() {
        return requestAt;
    }

    public void setRequestAt(String requestAt) {
        this.requestAt = requestAt;
    }

    public String getAcceptAt() {
        return acceptAt;
    }

    public void setAcceptAt(String acceptAt) {
        this.acceptAt = acceptAt;
    }

    @Override
    public String toString() {
        return "RequestUpdate{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", department='" + department + '\'' +
                ", position='" + position + '\'' +
                ", reason='" + reason + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", requestAt='" + requestAt + '\'' +
                ", acceptAt='" + acceptAt + '\'' +
                ", isAccept=" + isAccept +
                ", timeRemind=" + timeRemind +
                '}';
    }
}
