
package Entity.Response;


public class ResponseAPI<T> {
    String message;
    int code;
    T data;
    int totalPage;
    
    public ResponseAPI() {
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    @Override
    public String toString() {
        return "ResponseAPI{" +
                "message='" + message + '\'' +
                ", code=" + code +
                ", data=" + data +
                ", totalPage=" + totalPage +
                '}';
    }
}
