
package Entity.Response;

/**
 *
 * @author Tung 2508
 */
public class AddNewComment {
    int employeeId;
    String content;

    public AddNewComment() {
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
}
