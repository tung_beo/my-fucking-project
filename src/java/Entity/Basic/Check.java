/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity.Basic;

import java.sql.Time;
import java.util.Date;

/**
 *
 * @author Tung 2508
 */
public class Check {
    int requestId;
    int id;
    Date timeChecked;
    Time alarm;
    String status;
    String image;
    boolean confirm;
    Date confirmDate;

    public Check() {
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTimeChecked() {
        return timeChecked;
    }

    public void setTimeChecked(Date timeChecked) {
        this.timeChecked = timeChecked;
    }

    public Date getAlarm() {
        return alarm;
    }

    public void setAlarm(Time alarm) {
        this.alarm = alarm;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    public Date getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(Date confirmDate) {
        this.confirmDate = confirmDate;
    }

    @Override
    public String toString() {
        return "Check{" +
                "requestId=" + requestId +
                ", id=" + id +
                ", timeChecked=" + timeChecked +
                ", alarm=" + alarm +
                ", status='" + status + '\'' +
                ", image='" + image + '\'' +
                ", confirm=" + confirm +
                ", confirmDate=" + confirmDate +
                '}';
    }
}
