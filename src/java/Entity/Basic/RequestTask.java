
package Entity.Basic;

/**
 *
 * @author Tung 2508
 */
public class RequestTask {
    int reqId;
    int taskId;
    String status;
    String note;

    public RequestTask() {
    }

    public int getReqId() {
        return reqId;
    }

    public void setReqId(int reqId) {
        this.reqId = reqId;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "RequestTask{" +
                "reqId=" + reqId +
                ", taskId=" + taskId +
                ", status='" + status + '\'' +
                ", note='" + note + '\'' +
                '}';
    }
}
