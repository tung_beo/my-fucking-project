/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity.Basic;

/**
 *
 * @author Tung 2508
 */
public class Task {
    int taskId;
    int empCreateId;
    int fatherTaskId;
    String description;
    String title;
    boolean status;

    public Task() {
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int getEmpCreateId() {
        return empCreateId;
    }

    public void setEmpCreateId(int empCreateId) {
        this.empCreateId = empCreateId;
    }

    public int getFatherTaskId() {
        return fatherTaskId;
    }

    public void setFatherTaskId(int fatherTaskId) {
        this.fatherTaskId = fatherTaskId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskId=" + taskId +
                ", empCreateId=" + empCreateId +
                ", fatherTaskId=" + fatherTaskId +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", status=" + status +
                '}';
    }
}
