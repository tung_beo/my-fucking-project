/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity.Basic;

import java.util.Date;

/**
 *
 * @author Tung 2508
 */
public class Request {
    int requestId;
    int employeeId;
    Date startDateEs;
    Date endDateEs;
    String reason;
    Date startDateFact;
    Date endDateFact;
    String isAllow;
    String status;
    Date createDate;
    Date modifiedDate;
    Date seenDate;

    public Request() {
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Date getStartDateEs() {
        return startDateEs;
    }

    public void setStartDateEs(Date startDateEs) {
        this.startDateEs = startDateEs;
    }

    public Date getEndDateEs() {
        return endDateEs;
    }

    public void setEndDateEs(Date endDateEs) {
        this.endDateEs = endDateEs;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getStartDateFact() {
        return startDateFact;
    }

    public void setStartDateFact(Date startDateFact) {
        this.startDateFact = startDateFact;
    }

    public Date getEndDateFact() {
        return endDateFact;
    }

    public void setEndDateFact(Date endDateFact) {
        this.endDateFact = endDateFact;
    }

    public String getIsAllow() {
        return isAllow;
    }

    public void setIsAllow(String isAllow) {
        this.isAllow = isAllow;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date getSeenDate() {
        return seenDate;
    }

    public void setSeenDate(Date seenDate) {
        this.seenDate = seenDate;
    }

    @Override
    public String toString() {
        return "Request{" +
                "requestId=" + requestId +
                ", employeeId=" + employeeId +
                ", startDateEs=" + startDateEs +
                ", endDateEs=" + endDateEs +
                ", reason='" + reason + '\'' +
                ", startDateFact=" + startDateFact +
                ", endDateFact=" + endDateFact +
                ", isAllow='" + isAllow + '\'' +
                ", status='" + status + '\'' +
                ", createDate=" + createDate +
                ", modifiedDate=" + modifiedDate +
                ", seenDate=" + seenDate +
                '}';
    }
}
